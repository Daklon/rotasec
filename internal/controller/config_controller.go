/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"time"

	"github.com/robfig/cron"
	v1 "k8s.io/api/core/v1"

	"github.com/zach-klippenstein/goregen"
	rotasecinfov1alpha1 "gitlab.com/Daklon/rotasec/api/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// ConfigReconciler reconciles a Config object
type ConfigReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	Clock
}

type realClock struct{}

func (_ realClock) Now() time.Time { return time.Now() }

type Clock interface {
	Now() time.Time
}

//+kubebuilder:rbac:groups=rotasec.info,resources=configs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=rotasec.info,resources=configs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=rotasec.info,resources=configs/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=secrets/status,verbs=get

var (
	scheduleTimeAnnotation = "rotasec.info/scheduled-at"
)

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Config object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.16.0/pkg/reconcile
func (r *ConfigReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)

	//Get the current config object
	var config rotasecinfov1alpha1.Config
	if err := r.Get(ctx, req.NamespacedName, &config); err != nil {
		log.Error(err, "unable to fetch Config")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	//Check if the config is suspended
	if config.Spec.Suspend != nil && *config.Spec.Suspend {
		log.V(1).Info("config suspended, skipping")
		return ctrl.Result{}, nil
	}

	//Get the associated secret
	secret := &v1.Secret{}
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.NamespacedName.Namespace, Name: config.Spec.Secret}, secret); err != nil {
		log.Error(err, "unable to fetch the associated secret")
		return ctrl.Result{}, err
	}
	// function to get the next schedule
	getNextSchedule := func(config *rotasecinfov1alpha1.Config, now time.Time) (lastMissed time.Time, next time.Time, err error) {
		sched, err := cron.ParseStandard(config.Spec.Schedule)
		if err != nil {
			return time.Time{}, time.Time{}, fmt.Errorf("Unparseable schedule %q: %v", config.Spec.Schedule, err)
		}

		var earliestTime time.Time
		if config.Status.LastRotationTime != nil {
			earliestTime = config.Status.LastRotationTime.Time
		} else {
			earliestTime = config.ObjectMeta.CreationTimestamp.Time
		}
		if earliestTime.After(now) {
			return time.Time{}, sched.Next(now), nil
		}
		//revisar esto
		for t := sched.Next(earliestTime); !t.After(now); t = sched.Next(t) {
			lastMissed = t
		}
		return lastMissed, sched.Next(now), nil
	}
	//Get the next scheduled run
	missedRun, nextRun, err := getNextSchedule(&config, r.Now())
	if err != nil {
		log.Error(err, "unable to figure out config schedule")
		return ctrl.Result{}, nil
	}

	scheduledResult := ctrl.Result{RequeueAfter: nextRun.Sub(r.Now())}
	log = log.WithValues("now", r.Now(), "next run", nextRun)

	if missedRun.IsZero() {
		log.V(1).Info("no upcoming scheduled times, sleeping until next")
		return scheduledResult, nil
	}

	log = log.WithValues("current run", missedRun)
	// get the keys to rotate based on the specified includeKeys and excludeKeys
	keys := getKeysToRotate(config, secret)

	for i := 0; i < len(keys); i++ {
		value, _ := regen.Generate(config.Spec.Regex)
		secret.Data[keys[i]] = []byte(value)
	}
	r.Update(ctx, secret) //TODO improve, manage errors

	log.V(1).Info("Secret rotated")

	return scheduledResult, nil

}

func getKeysToRotate(config rotasecinfov1alpha1.Config, secret *v1.Secret) []string {
	var keys []string
	var keysToRotate []string
	secretKeys := make([]string, len(secret.Data))
	i := 0
	for k := range secret.Data {
		secretKeys[i] = k
		i++
	}
	lenSecretKeys := len(secretKeys)
	if len(config.Spec.IncludeKeys) > 0 {
		keys = config.Spec.IncludeKeys
		if len(keys) < 32 && lenSecretKeys < 32 {
			keysToRotate = intersectionNestedForBreak(secretKeys, keys)
		} else {
			keysToRotate = intersectionHashmap(secretKeys, keys)
		}
	} else if len(config.Spec.ExcludeKeys) > 0 {
		keys = config.Spec.ExcludeKeys
		if len(keys) < 32 && lenSecretKeys < 32 {
			keysToRotate = differenceNestedForBreak(secretKeys, keys)
		} else {
			keysToRotate = differenceHashmap(secretKeys, keys)
		}
	} else {
		keysToRotate = secretKeys
	}
	return keysToRotate
}

func intersectionNestedForBreak(keys1 []string, keys2 []string) []string {
	var result []string
	for _, element1 := range keys1 {
		for _, element2 := range keys2 {
			if element1 == element2 {
				result = append(result, element1)
				break
			}
		}
	}
	return result
}

func differenceNestedForBreak(keys1 []string, keys2 []string) []string {
	var result []string
	for _, element1 := range keys1 {
		pair_found := false
		for _, element2 := range keys2 {
			if element1 == element2 {
				pair_found = true
				break
			}
		}
		if !pair_found {
			result = append(result, element1)
		}
	}
	return result
}

func intersectionHashmap(keys1 []string, keys2 []string) []string {
	var result []string
	var keysIter1 []string
	var keysIter2 []string
	keysMap := make(map[string]bool)
	if len(keys2) > len(keys1) {
		keysIter1 = keys1[:]
		keysIter2 = keys2[:]
	} else {
		keysIter1 = keys2[:]
		keysIter2 = keys1[:]
	}
	for _, element := range keysIter1 {
		keysMap[element] = true
	}
	for _, element := range keysIter2 {
		if keysMap[element] {
			result = append(result, element)
		}
	}
	return result
}

func differenceHashmap(keys1 []string, keys2 []string) []string {
	var result []string
	var keysIter1 []string
	var keysIter2 []string
	keysMap := make(map[string]bool)
	if len(keys2) > len(keys1) {
		keysIter1 = keys1[:]
		keysIter2 = keys2[:]
	} else {
		keysIter1 = keys2[:]
		keysIter2 = keys1[:]
	}
	for _, element := range keysIter1 {
		keysMap[element] = true
	}
	for _, element := range keysIter2 {
		if !keysMap[element] {
			result = append(result, element)
		}
	}
	return result
}

// SetupWithManager sets up the controller with the Manager.
func (r *ConfigReconciler) SetupWithManager(mgr ctrl.Manager) error {
	if r.Clock == nil {
		r.Clock = realClock{}
	}
	return ctrl.NewControllerManagedBy(mgr).
		For(&rotasecinfov1alpha1.Config{}).
		Complete(r)
}
